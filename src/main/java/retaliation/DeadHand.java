package retaliation;

import org.jgroups.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.locks.ReentrantLock;

/**
 * This simple application is inspired by 'Dead Hand',
 * a semi-autonomous Soviet system designed to guarantee a nuclear retaliation
 * even if the country's leadership is completely obliterated.
 * It is speculated that this system is based upon a coordinated network
 * of radiation, seismic and other sensors, with a single coordinator node
 * that correlates the gathered data and makes a decision.
 */
public class DeadHand extends ReceiverAdapter {

    public static final String ABORT_SEQUENCE = "ABORT";
    public static final String TRIGGER_SEQUENCE = "TRIGGER";
    public static final String LAUNCH_SEQUENCE = "LAUNCH";

    private Address coordinatorAddress;
    private Channel channel;
    private boolean coordinator;

    private final Set<Address> triggers = new HashSet<Address>();
    private final ReentrantLock lock = new ReentrantLock();

    /*
    As per JGroups docs, a cluster's member list is always ordered and the first one is assumed to be a coordinator.
    When the coordinator node crashes, the next node in the list will take its place.
     */
    public void viewAccepted(View view) {
        lock.lock();

        try {
            establishLeadership(view);
        } finally {
            lock.unlock();
        }
    }

    private void establishLeadership(View view) {
        Address leader = view.getMembers().get(0);
        if (!coordinator && leader.equals(channel.getAddress())) {
            System.out.println("Becoming coordinator");
            coordinator = true;
        }

        if (!coordinator && (!leader.equals(coordinatorAddress))) {
            System.out.println("Selecting " + leader + " as coordinator");
            coordinatorAddress = leader;
        }
    }

    public void receive(Message msg) {
        Object payload = msg.getObject();
        Address sourceAddress = msg.getSrc();

        lock.lock();

        try {
            if (coordinator) {
                processSequence(payload, sourceAddress);
            } else {
                if (sourceAddress.equals(coordinatorAddress) && payload.equals(LAUNCH_SEQUENCE)) {
                    System.out.println("Launching retaliation strike!");
                }
            }
        } finally {
            lock.unlock();
        }
    }

    private void processSequence(Object payload, Address sourceAddress) {
        if (payload.equals(TRIGGER_SEQUENCE)) {
            if (!triggers.contains(sourceAddress)) {
                System.out.println("Trigger received from " + sourceAddress);
                triggers.add(sourceAddress);
            }

            if (triggers.size() >= 2) {
                // Why not quote the famous man?
                System.out.println("I am become death, the destroyer of worlds");
                broadcast(LAUNCH_SEQUENCE);
            }
        } else if (payload.equals(ABORT_SEQUENCE)) {
            if (triggers.contains(sourceAddress)) {
                System.out.println("Trigger aborted from " + sourceAddress);
                triggers.remove(sourceAddress);
            }
        }
    }

    public void start() throws Exception {
        channel = new JChannel("udp.xml");
        channel.setReceiver(this);
        channel.connect("DeadHand");
        readInput();
    }

    private void readInput() {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            try {
                System.out.print("> ");
                System.out.flush();
                String line = in.readLine();
                if (line.equals(TRIGGER_SEQUENCE) || line.equals(ABORT_SEQUENCE)) {
                    broadcast(line);
                } else {
                    System.out.println("Unrecognized command");
                }
            } catch (Exception ignored) {
            }
        }
    }

    private void broadcast(String message) {
        Message msg = new Message(null, null, message);
        System.out.println("Broadcasting message: " + message);
        try {
            channel.send(msg);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws Exception {
        new DeadHand().start();
    }
}
